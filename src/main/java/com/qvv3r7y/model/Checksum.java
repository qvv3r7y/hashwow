package com.qvv3r7y.model;

public class Checksum {
    private byte[] data;
    private String algorithm;
    private String check;
    private String checksum;
    private String equals;

    public Checksum(byte[] data, String algorithm, String check) {
        this.data = data;
        this.algorithm = algorithm;
        this.check = check;
    }

    public byte[] getData() {
        return data;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public String getCheck() {
        if (check == null) return "";
        return check;
    }

    public String getChecksum() {
        return checksum;
    }

    public String getEquals() {
        return equals;
    }

    public void setEquals(String equals) {
        this.equals = equals;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }
}
