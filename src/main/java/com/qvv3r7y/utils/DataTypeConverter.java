package com.qvv3r7y.utils;

public class DataTypeConverter {
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    private DataTypeConverter() {
        throw new IllegalStateException("Utils Class");
    }

    public static String getHexFromBytes(byte[] src) {
        char[] hexChars = new char[src.length * 2];
        for (int j = 0; j < src.length; j++) {
            int v = src[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
