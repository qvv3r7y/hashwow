package com.qvv3r7y;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.Controller;

@SpringBootApplication
@Controller
public class HashWowApplication {

	public static void main(String[] args) {
		SpringApplication.run(HashWowApplication.class, args);
	}

}

