package com.qvv3r7y.controller;

import com.qvv3r7y.model.Checksum;
import com.qvv3r7y.service.ChecksumService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/checksum")
public class ChecksumController {

    @GetMapping
    public String checksum() {
        return "../static/checksum";
    }

    @PostMapping
    public String uploadFile(@RequestParam("algorithm") String algorithm, @RequestParam("file") MultipartFile file, @RequestParam(name = "check", required = false) String check, Model model) {

        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                Checksum checksum = new Checksum(bytes, algorithm, check);
                ChecksumService service = new ChecksumService();
                service.checkChecksum(checksum);
                model.addAttribute("checksum", checksum);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "checksum";
    }

}
